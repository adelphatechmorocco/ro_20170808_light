
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]>
<html>
<!<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Why Webhelp</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/select2.min.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
</head>
<body class="aheight">
<div id="wrapper">
    <div id="content">
        <div class="nos-sectors-pr-page why-webhelp-page">
            <div class="nos-sectors-content why-wh-content">
                <a href="<?php print url("<front>"); ?>" class="btn-back">Retour a l'accueil</a>
                <h1 class="text-center">Why Webhelp ?</h1>
                <div class="box-sctors">
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/image-1-WHY-WH.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Multinational & Multicultural Environment</h3>
                                <p>Working in an international company has a lot of
                                    benefits and provides you a lot of new experiences. It
                                    increases your value as an employee, you have access to
                                    internal mobility, you can learn more about other
                                    cultures and values, you get to know new mindsets and
                                    perspectives, and also it’s easier to learn new languages!</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/IMAGE-2WHY-WB.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Continuous Personal Development & Training</h3>
                                <p>Your personal development and continuous training will
                                    help you assess your skills and qualities, having an
                                    important impact in your life and career goals. With this
                                    opportunity, you will be able to enhance your
                                    professional skills, maximising your potential, as well as
                                    to create a career path, with great opportunities to grow
                                    inside Webhelp.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/Image-1-BE-FR.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Workplace Conditions</h3>
                                <p>Webhelp has amazing working conditions that will
                                    provide you a safe working environment, a workplace
                                    full of natural light, open spaces and relaxing areas were
                                    you can chill and recharge your batteries, have your
                                    meals and enjoy your breaks with your colleagues and
                                    friends. We make sure that during your working hours,
                                    you will feel comfortable, rested and focused in your
                                    work, without having to worry about your well being and
                                    safety.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/Image-4-Why-Wb.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Long-Term Employment</h3>
                                <p>We are eager to keep our employees happy and willing
                                    to continue within Webhelp’s family for a long-term.
                                    With long-term employment you will be able to achieve
                                    a seniority level within the company which will bring you
                                    leadership opportunities with the chance of increased
                                    benefits. The long-term employment will also mean a good worklife 
                                    balance, due to a stable life and career
                                    path that you will be able to achieve.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/Image2-bIEN-ETRE.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Supportive and Friendly Atmosphere</h3>
                                <p>Webhelp will offer you a friendly environment, where
                                    you will feel confortable sharing your ideas, providing
                                    suggestions for improvement, participating in our
                                    activities with your colleagues, supervisors and
                                    management members. We make all the efforts needed
                                    to provide an inclusive environment were everybody can
                                    be part of those activities, where you can connect and
                                    discover new interests, as well as making new
                                    friendships!</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/videoLightning.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
</body>

</html>
