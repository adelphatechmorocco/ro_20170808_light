<?php
global $user;
if ( $user->uid && in_array('administrator', $user->roles) ) {
  
} else {
    drupal_goto('/');
}
if(!empty($_POST['du'])){
	$du = DateTime::createFromFormat('d/m/Y', $_POST['du']);
	$du = $du->format('Y-m-d');
	$au = DateTime::createFromFormat('d/m/Y', $_POST['au']);
	$au = $au->format('Y-m-d');
     $result = db_query("SELECT * FROM wh_candidature
                        WHERE created>='".strtotime($du)."' AND created<'".strtotime($au)."';"
                      );
    $nbr = $result->rowCount();
}
?>
<link rel="stylesheet" type="text/css" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.datepick.css">
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.datepick.js"></script>

<?php include './'. path_to_theme() .'/templates/page/header.tpl.php'; ?>



<div class="page-<?php print $node->nid?>">
 <?php 
     print $messages;
 ?>
 <div class="espace-candidature">
    <div class="container-espace-candidature">
        <form action="" method="post" class="form-wrapper" name="ajout-creneau">
            <div class="contenu-form">
                <div>
                    <?php if(!empty($nbr)){ ?>
                        <div class="success" style="width:100%; color:green; text-align:center;font-size: 24px; display: block; float: left; margin-bottom: 100px;">Le nombre de candidature pour cette période est: <?php print $nbr; ?></div>
                    <?php } ?>
                    <p>Nombre des candidatures<br /><br /></p>
                    
                    <div class="form-group">
                        <input type="text" name="du" placeholder="Du (DD/MM/YYYY)" required="true" class="required date" style="width:100%">
                    </div>
                    <div class="form-group">
                        <input type="text" name="au" placeholder="Au (DD/MM/YYYY)" required="true" class="required date" style="width:100%">
                    </div>
                </div>
            </div>
            <div class="buttons">
                <button type="submit" class="postuler btn">Envoyer</button>
            </div>
        </form>
        <div class="entretien-video">
            <a href="#"><p class="text-center">Présentez vous et passez votre entretien en vidéo</p></a>
        </div>
    </div>
</div>

<script>
$('.date').datepick({dateFormat: 'yyyy-mm-dd'});
</script>
 </div>

<?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>
<script type="text/javascript">
		var bh = jQuery(".page-<?php print $node->nid; ?>").html().replace(/[\u200B]/g, '');
		jQuery(".page-<?php print $node->nid; ?>").html(bh);
		jQuery("#wrapper").css('float','left');	
</script>
