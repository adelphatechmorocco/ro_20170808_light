<?php
/**
 * @file
 * Themes the question report
 *
 * Available variables:
 * $form - FAPI array
 *
 * All questions are in form[x] where x is an integer.
 * Useful values:
 * $form[x]['question'] - the question as a FAPI array(usually a form field of type "markup")
 * $form[x]['score'] - the users score on the current question.(FAPI array usually of type "markup" or "textfield")
 * $form[x]['max_score'] - the max score for the current question.(FAPI array of type "value")
 * $form[x]['response'] - the users response, usually a FAPI array of type markup.
 * $form[x]['#is_correct'] - If the users response is correct(boolean)
 * $form[x]['#is_evaluated'] - If the users response has been evaluated(boolean)
 */
// $td_classes = array('quiz-report-odd-td', 'quiz-report-even-td');
// $td_class_i = 0;
$p = drupal_get_path('module', 'quiz') . '/theme/';
$q_image = $p . 'question_bg.png';
?>
<script type="text/javascript">
    jQuery("document").ready(function(){
        
        if(jQuery(".result").length){ 
            jQuery(' h2.finalReport').show();
            jQuery('h2.reportForm').hide();
            jQuery("#quiz-report-form .quiz-report-question strong").show();
        }else{
            jQuery(' h2.finalReport').hide();
            jQuery('h2.reportForm').show();
            jQuery("#quiz-report-form .quiz-report-question strong").hide();
        }
        
        var blocks=[];  
        var i=0;
        (document.getElementById('quiz-report-form'))?blocks[i++]=document.getElementById('quiz-report-form'):"";
        (document.getElementById('quiz_progress') )?blocks[i++]=document.getElementById('quiz_progress'):"";
        (document.getElementById('quiz-question-answering-form'))?blocks[i++]=document.getElementById('quiz-question-answering-form'):"";
           
        reorderElements(blocks);
    })
        
    function reorderElements(elems) {
        var count = elems.length;
        if (!count) {
            return;
        }
        
        var parent = elems[0].parentNode;
        if(count>1){
            for (var i = count - 1; i >= 0; i--) {
                parent.insertBefore(elems[i], parent.firstChild);
            }
        }
    }
</script>
<h2 class="finalReport"><?php print t('Résultats du quiz'); ?></h2>
<h2 class="reportForm"><?php print t('Résultats de la question précedente'); ?></h2>

<dl class="quiz-report">

    <?php
    $totalQuestion = 0;
    foreach ($form as $key => $sub_form) {
        if (!is_numeric($key) || isset($sub_form['#no_report']))
            continue;
        $totalQuestion++;
    }

    $currentQuestion = 1;
    foreach ($form as $key => $sub_form):
        if (!is_numeric($key) || isset($sub_form['#no_report']))
            continue;
        unset($form[$key]);
        $c_class = ($sub_form['#is_evaluated']) ? ($sub_form['#is_correct']) ? 'q-correct' : 'q-wrong'  : 'q-waiting';
        $skipped = $sub_form['#is_skipped'] ? '<span class="quiz-report-skipped">' . t('(skipped)') . '</span>' : ''
        ?>

        <dt>
        <div class="quiz-report-score-container <?php print $c_class ?>">
            <span>
                <?php print t('Score') ?>
                <?php print drupal_render($sub_form['score']) ?>
                <?php print t('of') . ' ' . $sub_form['max_score']['#value'] ?>
                <?php print '<br><em>' . $skipped . '</em>' ?>
            </span>
        </div>

        <p class="quiz-report-question"><strong><?php print t('Question %currentQuestion sur %totalQuestion', array('%currentQuestion' => $currentQuestion++, '%totalQuestion' => $totalQuestion)) ?>: </strong></p>
        <?php print drupal_render($sub_form['question']); ?>
        </dt>

        <dd>
            <p><strong><?php print t('Response') ?>: </strong></p>


            <?php print drupal_render($sub_form['response']); ?>
        </dd>

    <?php endforeach; ?>
</dl>

<div style="float:right;"><?php print drupal_render_children($form); ?></div>
