
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]>
<html>
<!<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Pourquoi choisir</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/select2.min.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
</head>
<body class="aheight">
<div id="wrapper">
    <div id="content">
        <div class="nos-sectors-pr-page why-webhelp-page">
            <div class="nos-sectors-content why-wh-content">
                <a href="<?php print url("<front>"); ?>" class="btn-back">Retour a l'accueil</a>
                <h1 class="text-center">Pourquoi Webhelp ?</h1>
                <div class="box-sctors">
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/image-1-WHY-WH.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Un Groupe International </h3>
                                <p>Travailler dans une entreprise internationale vous offre de nouvelles expériences: adhérer à des valeurs fortes et vivre ensemble la réussite d’une entreprise en pleine croissance . Vous avez accès à la mobilité interne, vous pouvez en apprendre davantage sur d'autres cultures et valeurs, apprendre de nouvelles mentalités et perspectives, et aussi apprendre plus facilement de nouvelles langues!
</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/IMAGE-2WHY-WB.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Accompagnement et perspectives d’évolution </h3>
                                <p>Chez Webhelp, vous bénéficiez des meilleurs formations grâce à l’expertise de la Webhelp university consacrée à votre  développement professionnel . Des formations continues qui vous aideront  à évaluer vos compétences et vos qualités. Avec cette opportunité, vous serez en mesure d'améliorer vos compétences professionnelles, maximiser votre potentiel, ainsi que de créer une carrière, au sien de Webhelp.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/Image-1-BE-FR.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Des conditions de travail favorables </h3>
                                <p>Chez Webhelp vous travaillez dans une entreprise ou il fait bon vivre. Webhelp prends en compte le bien-être de ses collaborateurs au travail et leur offre une qualité de vie agréable au travail. Un environnement de travail moderne:  des espaces ouverts et des zones de détente où vous pouvez recharger vos batteries, prendre vos repas et profiter de vos pauses avec vos collègues et amis. Nous nous assurons que pendant vos heures de travail, vous vous sentirez à l'aise, reposé et concentré dans votre travail, sans avoir à vous soucier de votre bien-être et la sécurité.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/Image-4-Why-Wb.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Un emploi à long terme </h3>
                                <p>Nous sommes heureux de garder nos collaborateurs heureux et disposés à continuer dans la famille de Webhelp. Avec l'emploi à long terme, vous serez en mesure d'atteindre un niveau de séniorité au sein de l'entreprise qui vous apportera des opportunités d’évolution et des  d'avantages inégalés . L'emploi à long terme se traduira également par un bon équilibre vie / travail, en raison d'une vie stable et une carrière que vous serez en mesure d'atteindre.
</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-why-wh">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/Image2-bIEN-ETRE.png" alt="" class="center-block">
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Une ambiance conviviale </h3>
                                <p>Webhelp vous offre un environnement convivial, où vous vous sentirez à l'aise de partager vos idées, de proposer des suggestions d'amélioration, de participer à nos activités avec vos collègues, les superviseurs et les membres de la direction. Nous faisons mettons tout en œuvre pour garantir une atmosphère agréable où vous pouvez vous connecter et découvrir de nouveaux intérêts, ainsi que de faire de nouvelles amitiés!</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/videoLightning.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
</body>

</html>
