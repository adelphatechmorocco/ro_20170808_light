<?php /*************** WEBHELP PORTUGAL EN ***************/ ?>
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Webhelp Romania</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/owl.theme.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/custom.css">
</head>

<body class="aheight">
<div id="wrapper">
    <div id="content">
        <div class="webhelp-maroc webhelp-france webhelp-portugal">
            <div class="webhelp-maroc-content webhelp-france-content webhelp-portugal-content">
                <a href="<?php print url("<front>"); ?>" class="btn-back">Return to home page</a>
                <h1 class="text-center">Webhelp Romania</h1>
                <p class="leader-text text-center">A worldwide leader in Customer Relationship Management, Webhelp Romania is:</p>
                <div class="temoignage-maroc temoignage-portugal">
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/collaborateurs-maroc.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">2 000</span><span class="plus">+</span>
                            <span class="criteres-temoignage"> Employees</span>
                        </div>
                    </div>
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-ville.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">5</span>
                            <span class="criteres-temoignage">Locations</span>
                        </div>
                    </div>
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/site-production.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">6</span>
                            <span class=" criteres-temoignage">Sectors</span>
                        </div>
                    </div>
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-client.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">24</span>
                            <span class="plus">+</span>
                            <span class="criteres-temoignage">Clients</span>
                        </div>
                    </div>
                    <div class="temoignage-item">
                        <div class="img-temoignage">
                            <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-lang.png" class="center-block">
                        </div>
                        <div class="text-temoignage">
                            <span class="counter">14</span>
                            <span class="criteres-temoignage">Languages</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="our-values">
                <h4 class="text-center">Our Values</h4>
                <p class="text-center">Guidelines to the way we should treat one another and achieve our goals :</p>
                <div class="items-values">
                    <div class="item commitment">
                        <div class="img-value">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-commitment.png" class="center-block" alt="">
                        </div>
                        <p>Commitment</p>
                        <div class="text-hover">
                            <p>We honour the promises made to colleagues, clients and customers.</p>
                        </div>
                    </div>
                    <div class="item recognition">
                        <div class="img-value">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-recognition.png" class="center-block"  alt="">
                        </div>
                        <p>Recognition</p>
                        <div class="text-hover">
                            <p>Acknowledgement of individual contribution is essential to ensure our collective success. If you share our values, we'd like to share details of our current opportunities with you! </p>
                        </div>
                    </div>
                    <div class="item unity">
                        <div class="img-value">
                            <img class="center-block" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-unity.png"  alt="">
                        </div>
                        <p>Unity</p>
                        <div class="text-hover">
                            <p>Working as one team towards a common success takes precedence over personal interest.</p>
                        </div>
                    </div>
                    <div class="item integrity">
                        <div class="img-value">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-integrity.png" class="center-block"  alt="">
                        </div>
                        <p>Integrity</p>
                        <div class="text-hover">
                            <p>We work with a positive attitude leading by example. We treat others the way we would expect to be treated.</p>
                        </div>
                    </div>
                    <div class="item woww">
                        <div class="img-value">
                            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-wow.png" class="center-block" alt="">
                        </div>
                        <p>Wow</p>
                        <div class="text-hover">
                            <p>By 'going the extra mile' and embracing innovation, we continually seek to amaze the people we work with !</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="webhelp-ville">
                <p class="text-center">Webhelp Romania's locations are situated in :</p>
                    <div class="villes-items" data-url-page="<?php print url('node/59042')?>">
                       <div class="ville-nom">
                        <a href="#" class="fancybox" data-ville="bucarest1" data-lat="44.446670" data-lng="26.089416"><p>Bucharest 1</p></a>
                    </div>
                    <div class="ville-nom">  
                        <a href="#" class="fancybox" data-ville="bucarest2" data-lat="44.451853" data-lng="26.080944"><p>Bucharest 2</p></a>
                    </div>                    
					<div class="ville-nom">
                        <a href="#" class="fancybox" data-ville="iasi" data-lat="47.159301" data-lng="27.5862109"><p>Iasi</p></a>
                    </div>					
					<div class="ville-nom">
                        <a href="#" class="fancybox" data-ville="ploiesti" data-lat="44.9122412" data-lng="26.0369732"><p>Ploiesti</p></a>
                    </div>					
					<div class="ville-nom">
                        <a href="#" class="fancybox" data-ville="galati" data-lat="45.4362136" data-lng="28.034512"><p>Galati</p></a>
                    </div>
                    </div>
                <div class="slider-locaux">
                    <p class="text-center">Visit our offices’ modern and high standing buildings :</p>
                    <div class="webhelp-maroc-carousel">
                        <ul>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro1.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro2.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro3.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro4.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro5.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro6.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro7.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro8.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro9.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro10.jpg" class="center-block"> </li>
							  <li> <img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/whro11.jpg" class="center-block"> </li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<script type="text/javascript" src="js/jquery.counterup.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/owl.carousel.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDoPCU-YYlKiwPqkSb_PV6ZDBquzApMdwo"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/script.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
</body>
</html>
