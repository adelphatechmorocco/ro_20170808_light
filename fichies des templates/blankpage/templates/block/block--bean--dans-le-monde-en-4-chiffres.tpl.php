<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['dans-le-monde-en-4-chiffres']['#entity'];
$el2 = $elements['bean']['dans-le-monde-en-4-chiffres']['field_chiffres'];
$n = count($el2['#items']);
?>
<div class="temoignage-monde four-figures <?php print $classes; ?>" style="background-position: center -994px;" <?php print $attributes; ?>>
    <?php
    print render($title_suffix);
    ?>
    <h2 class="text-center wow zoomIn"><?php print $el->title; ?></h2>
    <div class="content-temoignage-monde">
        <?php
        for($i=0; $i<$n; $i++):
            $m=$i+1;
            $index = $el2['#items'][$i]['value'];
            $img=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']);
            $lastc = substr($el2[$i]['entity']['field_collection_item'][$index]['field_chiffre']['#items'][0]['value'],-1);
            $s = "";
            if(!is_numeric($lastc)){
                $chiffre = substr($el2[$i]['entity']['field_collection_item'][$index]['field_chiffre']['#items'][0]['value'],0,-1);
                if(!is_numeric($chiffre)){

                }else{

                    if($lastc=="+"){
                        $s = '<span class="plus">+</span>';
                    }else{
                        $s = '<span class="letter-ca">'.$lastc.'</span>';
                    }
                }

            }else{
                $chiffre = $el2[$i]['entity']['field_collection_item'][$index]['field_chiffre']['#items'][0]['value'];
            }
            ?>
            <div class="temoignage-item">
                <div class="img-temoignage">
                    <img alt="" src="<?php print $img?>" class="center-block">
                </div>
                <div class="text-temoignage">
                    <span class="counter"><?php print $chiffre?></span><?php print $s?>
                    <span class="criteres-temoignage"><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></span>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>
