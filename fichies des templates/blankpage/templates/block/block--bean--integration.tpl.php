<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['integration']['#entity'];
$el2 = $elements['bean']['integration']['field_onglets'];
$n = count($el2['#items']);
$el3 = $elements['bean']['integration']['field_boutons'];
$m = count($el3['#items']);
?>
<div class="integration get-board block block-bean odd integration-france <?php print $classes; ?>" id="integration" <?php print $attributes; ?>> 
    <?php
    print render($title_suffix);
    ?>
    <div class="container">
        <div class="content-integration">
            <h2 class="text-center wow zoomIn"><?php print $el->title; ?></h2>
            <p class="integration-text wow zoomIn"><?php print $el->field_description[$lang_name]['0']['value']; ?></p>


            <div class="item-get-board">
                <?php
                for($i=0; $i<$n; $i++):
                    $index = $el2['#items'][$i]['value'];
                    $pon=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']);
                    //$poff=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_off']['#items'][0]['uri']);
                    if($i%2==0){
                        $c="fadeInUp";
                    }else{
                        $c="fadeInDown";
                    }
                    //kpr($el2[$i]['entity']['field_collection_item'][$index]);
                    ?>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding text-center zoomIn animated wow">
                        <div class="img-board">
                            <img src="<?php print $pon?>" alt="">
                        </div>
                        <h3><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></h3>
                        <p class="text-center"><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_description']['#items'][0]['value']?></p>
                    </div>
                <?php endfor; ?>
            </div>
            <div class="bottom-get-board">
                <h3 class="text-center"><?php print $el->field_titre_2[$lang_name]['0']['value']; ?></h3>
                <p class="text-center"><?php print $el->field_autre[$lang_name]['0']['value']; ?></p>
                <div class="video-get-board">
                    <img src="<?php print file_create_url($el->field_image[LANGUAGE_NONE]['0']['uri'])?>" alt="" class="img-responsive img-video wow zoomIn center-block">
                    <span class="video-link" data-video-id="<?php print $el->field_sous_titre[$lang_name]['0']['value']; ?>" data-video-width="1100" data-video-height="650"><img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-get-board.png" class="zoomIn"></span>
                </div>
                <div class="services-webhelp services-webhelp-pr">
                    <?php
                    for($i=0; $i<$m; $i++):
                        $j=$i+1;
                        $index = $el3['#items'][$i]['value'];
                        $pon=file_create_url($el3[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']);
                        $poff=file_create_url($el3[$i]['entity']['field_collection_item'][$index]['field_picto_off']['#items'][0]['uri']);
                        ?>
                        <div class="services-item">
                            <div class="content-services-item">
                                <a href="<?php print url($el3[$i]['entity']['field_collection_item'][$index]['field_lien']['#items'][0]['value'])?>">
                                    <div class="imgs cf">
                                        <img alt="" class="top" src="<?php print $poff?>">
                                        <img alt="" class="bottom" src="<?php print $pon?>">
                                    </div>
                                    <span><?php print $el3[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></span>
                                </a>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</div>