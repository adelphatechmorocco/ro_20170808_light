<?php
global $language ;
$lang_name = $language->language;
$el = $elements['bean']['job-openings-by-sector']['#entity'];
$el2 = $elements['bean']['job-openings-by-sector']['field_emplois_par_secteur'];
$n = count($el2['#items']);
?>

<div  id="newsletter" class="job-sector <?php print $classes; ?>" <?php print $attributes; ?>>
    <?php
    print render($title_suffix);
    ?>
    <h2 class="text-center"><?php print $el->title; ?></h2>
    <div class="content-job-sector">
        <?php
        for($i=0; $i<$n; $i++):
            $index = $el2['#items'][$i]['value'];
            $img=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_image']['#items'][0]['uri']);

            $url = explode("?", $el2[$i]['entity']['field_collection_item'][$index]['field_lien']['#items'][0]['value']);
            $params = explode("=",$url[1]);
            ?>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding text-center zoomIn animated wow">
            <div class="job-sector-item">
                <div class="img-job">
                    <img src="<?php print $img?>" alt="">
                </div>
                <h3><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></h3>
                <a href="<?php print url($url[0],array('query' => array($params[0] => $params[1])))?>"><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_titre_du_bouton']['#items'][0]['value']?></a>
            </div>
        </div>
        <?php endfor; ?>
    </div>
</div>