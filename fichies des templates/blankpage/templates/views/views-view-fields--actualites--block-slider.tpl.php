<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
//$node = node_load($row->nid);
$image_actualite = "";
if (isset($row->_field_data['nid']['entity']->field_image_actualit_['und']))
    $image_actualite = file_create_url($row->_field_data['nid']['entity']->field_image_actualit_['und'][0]['uri']);
$nid = strip_tags($fields['nid']->content);
?>
<li class="item" data-transition="fade" data-slotamount="7" data-masterspeed="1500">
<span class="shadow-top"></span>
<img src="<?php print $image_actualite ?>" alt="">
<span class="shadow-bottom"></span>
<a href="<?php print url('node/'.$nid); ?>">
<div class="tp-caption small_thin_grey customin customout"
    data-x="center" data-hoffset="0"
    data-y="bottom" data-voffset="-400"
    data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
    data-speed="1200"
    data-start="1000"
    data-easing="Power4.easeOut"
    data-endspeed="500"
    data-endeasing="Power4.easeIn"
    data-captionhidden="on"
    style="z-index: 1"><h2 class="text-center"><?php print $fields['field_titre_2']->content; ?></h2>
</div>
<div class="tp-caption small_thin_grey customin customout"
    data-x="center" data-hoffset="0"
    data-y="bottom" data-voffset="-340"
    data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
    data-speed="1200"
    data-start="1600"
    data-easing="Power4.easeOut"
    data-endspeed="500"
    data-endeasing="Power4.easeIn"
    data-captionhidden="on"
    style="z-index: 2"><p class="text-center"><?php print html_entity_decode($fields['field_sous_titre_slider']->content); ?></p>
</div>
</a>
</li>