
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]>
<html>
<!<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Nos Secteurs</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/select2.min.css">
    <link rel="stylesheet" href="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/modernizr.js"></script>
</head>
<body class="aheight">
<div id="wrapper">
    <div id="content">
        <div class="nos-sectors-pr-page">
            <div class="nos-sectors-content">
                <a href="<?php print url("<front>"); ?>" class="btn-back">Retour a l'accueil</a>
                <h1 class="text-center">Nos Secteurs</h1>
                <div class="box-sctors">
                    <div class="box-item">
                        <div class="img-sctor">
                            <a href="<?php print url("offre-emploi", array('query' => array( "field_secteur_tid" => "47"))); ?>"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-telco.png" alt="" class="center-block"></a>
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Voyage & Tourisme </h3>
                                <p>Nous comptons parmi nos clients des tour-opérateurs, des groupes hôteliers et des centres de vacances, ainsi que différentes entreprises du transport et des loisirs.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-sctor">
                            <a href="<?php print url("offre-emploi", array('query' => array( "field_secteur_tid" => "45"))); ?>"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-tourism.png" alt="" class="center-block"></a>
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Télécom & IT </h3>
                                <p>Webhelp travaille, au sein du secteur télécom, avec un nombre de marques important, qu'il s'agisse d'acteurs spécialisés uniquement dans les télécommunications, d'experts du divertissement ou encore d'entreprises de plus en plus nombreuses offrant des services combinés.</p>
                            </div>
                        </div>
                    </div>
                    <div class="box-item">
                        <div class="img-sctor">
                            <a href="<?php print url("offre-emploi", array('query' => array( "field_secteur_tid" => "46"))); ?>"><img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-consumer-goods.png" alt="" class="center-block"></a>
                        </div>
                        <div class="text-sector">
                            <div class="inner-text">
                                <h3>Retail </h3>
                                <p>Webhelp, grâce à une approche omnicanal (téléphone, mail, online, …), donne la possibilité à ses clients du secteur de gagner le cœur des clients, de les convaincre et de générer des parts de marché.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/videoLightning.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
<script src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js"></script>
</body>

</html>
