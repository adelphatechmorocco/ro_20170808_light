<?php
global $language ;

$lang_name = $language->language;
$el = $elements['bean']['bouton-utiles']['#entity'];
$el2 = $elements['bean']['bouton-utiles']['field_boutons'];
$n = count($el2['#items']);
?>
<div class="actu-job <?php print $classes; ?>" <?php print $attributes; ?> id="actujob">
    <?php
    print render($title_suffix);
 
    ?>
    <div class="top-actu-job">
        <h2 class="text-center wow zoomIn"><?php print $el->title; ?><a href="<?php print url('offre-emploi');?>"><?php print t("new"); ?></a></h2>
	    <?php 
		    $block = module_invoke('views', 'block_view', 'offre_emploi-actu_job');
			print render($block['content']); 
	    ?>
    </div>
    <div class="bottom-actu-job">
       <?php 
       for($i=0; $i<$n; $i++):
       $m=$i+1;
       $index = $el2['#items'][$i]['value'];
       $pon=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']); 
       $poff=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_off']['#items'][0]['uri']);
       ?>
        <div class="actu-job-item">
            <div class="content-actu-job-item wow zoomIn"  data-wow-duration="2s">
                <a href="<?php print url($el2[$i]['entity']['field_collection_item'][$index]['field_lien']['#items'][0]['value']);?>" class="image-link">
                    <div class="imgs cf">
                        <img alt="" class="top" src="<?php print $poff?>">
                        <img alt="" class="bottom" src="<?php print $pon?>">
                    </div>
                    <span><?php print $el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></span>
                </a>
            </div>
        </div>
        <?php 
        endfor; 
        ?>
    </div>
</div>