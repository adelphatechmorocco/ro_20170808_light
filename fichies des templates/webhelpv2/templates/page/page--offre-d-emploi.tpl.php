<?php 
global $user;
global $language ;
$lang_name = $language->language;
$term = taxonomy_term_load($node->field_categorie_actualite[LANGUAGE_NONE][0][tid]);
$name = $term->name;
?>
<header class="slide-pages">
    <div class="top-slide top-slide-offres">
        <?php
        $titre = '<div class="titre">
                <h1 class="text-center">'.t("Our Opportunities").'</h1>
                <h2 class="text-center">'.t("Find the job of your dream !").'</h2>
                </div>';
        include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
            </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
               </header>

               <div id="content">
                
                <div class="content-page">
                    <div class=" top-page-content">
                       
                       <?php 
                       if($user->uid && in_array('administrator', $user->roles))
                           print l(t('Editer'), 'node/'.$node->nid.'/edit', array('attributes' => array('class' => 'editlink')));
                    ?>
                    <div id="node-body">
                       <div class="details-offre">
                        <div class="content-details-offres">
                            <?php 
		                       print $messages; 
		                       ?>
                            
                            <h2><?php print $node->title?></h2>
                            <div class="top-details-offres">
                                <?php 
                                $term = taxonomy_term_load($node->field_ville[LANGUAGE_NONE][0][tid]);
                                $ville = $term->name;   
                                $term2 = taxonomy_term_load($node->field_metier[LANGUAGE_NONE][0][tid]);
                                $metier = $term2->name;
                                $term3 = taxonomy_term_load($node->field_secteur[LANGUAGE_NONE][0][tid]);
                                $secteur = $term3->name; 
                                $term4 = taxonomy_term_load($node->field_langue_offre[LANGUAGE_NONE][0][tid]);
                                $langue = $term4->name; 
                                if($node->field_contrat[LANGUAGE_NONE][0][value]==0)
                                    $contrat="ANAPEC";
                                elseif($node->field_contrat[LANGUAGE_NONE][0][value]==1)
                                    $contrat="CDD";
                                else
                                    $contrat="CDI";
                                ?>
                                <p><span><?php print t('City'); ?> :</span> <?php print $ville?> </p>
                                <p><span><?php print t('Job'); ?> :</span> <?php print $metier?></p>
                                <p><span><?php print t('Sector'); ?> :</span> <?php print $secteur?></p>
								<!--p><span>Langue :</span> <?php print $langue?></p-->
                                <!--p><span>Contrat :</span> <?php print $contrat?></p-->
                                <?php
                                if ($logged_in):
                                    $nid = getNodeByUidByType($user->uid, "cv_webhelp");
                                ?>
                                <?php if ($nid): ?>
                                    <?php 
                                    if(!empty($_GET['ref'])){
                                        switch ($_GET['ref']) {
                                            case 'www.viadeo.com':
                                            $ref =  "viad";
                                            break;
                                            case 'www.facebook.com':
                                            $ref = "fc";
                                            case 'l.facebook.com':
                                            $ref = "fc";
                                            break;
                                            case 'www.youtube.com':
                                            $ref = "ytb";
                                            break;
                                            case 'www.linkedin.com':
                                            $ref = "in";
                                            break;  
                                            case 'google.com':
                                            $ref = "gle";
                                            break;      
                                        }
                                    }
                                    
                                    if(!empty($_GET['w']))  $w = base_path().$_GET['w'];
                                    if(!empty($ref)) $ref  = base_path().$ref;
                                    
                                    ?>
                                    
                                    <div class="link-postuler">
                                        <a href="<?php print url('recrute/postuler/' . arg(1) . $ref . $w); ?>"><?php print t('Apply'); ?></a>
                                    </div>
                                <?php else : ?>
                                    <div class="link-postuler">
                                        <a href="<?php print url('cv-webhelp'); ?>"><?php print t('Apply'); ?></a>
                                    </div>
                                <?php endif; ?>

                            <?php else : ?>
                                <div class="link-postuler">
                                    <a href="<?php print url('user/login'); ?>"><?php print t('Apply'); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php  print render($node->body[LANGUAGE_NONE][0][value]); ?>
                        <br> <br>
                        <div class="top-details-offres last">
                           <?php   if ($logged_in):
                           $nid = getNodeByUidByType($user->uid, "cv_webhelp");
                           ?>
                           <?php if ($nid): ?>
                            <?php 
                            if(!empty($_GET['ref'])){
                                switch ($_GET['ref']) {
                                    case 'www.viadeo.com':
                                    $ref =  "viad";
                                    break;
                                    case 'www.facebook.com':
                                    $ref = "fc";
                                    case 'l.facebook.com':
                                    $ref = "fc";
                                    break;
                                    case 'www.youtube.com':
                                    $ref = "ytb";
                                    break;
                                    case 'www.linkedin.com':
                                    $ref = "in";
                                    break;  
                                    case 'google.com':
                                    $ref = "gle";
                                    break;      
                                }
                            }
                            
                            if(!empty($_GET['w']))  $w = base_path().$_GET['w'];
                            if(!empty($ref)) $ref  = base_path(). $ref;
                            
                            ?>
                            
                            <div class="link-postuler">
                                <a href="<?php print url('recrute/postuler/' . arg(1) . $ref . $w); ?>"><?php print t('Apply'); ?></a>
                            </div>
                        <?php else : ?>
                            <div class="link-postuler">
                                <a href="<?php print url('cv-webhelp'); ?>"><?php print t('Apply'); ?></a>
                            </div>
                        <?php endif; ?>

                    <?php else : ?>
                        <div class="link-postuler">
                            <a href="<?php print url('user/login'); ?>"><?php print t('Apply'); ?></a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="retour"><a href="<?php print url('offre-emploi');?>"><?php print t("Return to list"); ?></a></div>
            </div>
        </div>
        
    </div>
</div>
</div>
<?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>
</div>


