<?php
global $user;
global $language ;
$lang_name = $language->language;

?>
<footer id="footer" class="footer-france">
    <div class="footer-top" id="footertop">
        <div class="content-footer footer-pages">
            <div class="logo-footer wow zoomIn animated" style="visibility: visible; animation-name: zoomIn;">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo-footer.png" alt="" class="logo-foot center-block">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/images/map-footer.png" alt="" class="img-responsive map-footer-top">
            </div>
            <div class="menu-footer wow zoomIn animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: zoomIn;">
                <?php if($lang_name=='fr'){ ?>
                <ul>
                    <li class="menu-items menu-items-wh">
                        <a>Qui sommes-nous ?</a>
                        <ul>
                            <li><a href="<?php print url('webhelp-roumanie')?>">Webhelp Roumanie</a></li>
                            <li><a href="http://webhelp.com/">Webhelp Monde</a></li>
                            <li><a href="<?php print url('pourquoi-webhelp')?>">Pourquoi Webhelp ?</a></li>
                            <li><a href="<?php print url('actualites')?>">Actus recrutement</a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-avenir">
                        <a>Ton avenir avec nous</a>
                        <ul>
                            <li><a href="<?php print url('nos-secteurs')?>">Nos Secteurs</a></li>
                            <li><a href="<?php print url('nos-metiers')?>">Nos Métiers</a></li>
                            <li><a href="<?php print url('webhelp-university')?>">Formation</a></li>
                            <li><a href="<?php print url('talent-management')?>">Évolution</a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-famille">
                        <a>Ta nouvelle famille</a>
                        <ul>
                            <li><a href="<?php print url('bien-etre')?>">Bien-être</a></li>
                            <li><a href="<?php print url('avantages')?>">Avantages et Bénéfices</a></li>
                            <li><a href="<?php print url('webhelp-culture')?>">Notre culture</a></li>
                            <li><a href="<?php print url('fun')?>">Fun </a></li>
                          <!--  <li><a href="<?php print url('fun-0')?>">Solidarité</a></li> -->
                        </ul>
                    </li>
                    <li class="menu-items menu-items-aventure">
                        <a>Envie de nous rejoindre</a>
                        <ul>
                            <li><a href="<?=url('<front>');?>#postuler-webhelp">Recrutement</a></li>
                            <li><a href="<?=url('<front>');?>#integration">Intégration</a></li>
                            <li><a href="<?php print url('offre-emploi')?>">Nos offres</a></li>
                            <li><a href="<?php print url('user/login')?>">Rejoignez-nous</a></li>
                        </ul>
                    </li>
                </ul>
                <?php }elseif($lang_name=='ro'){ ?>
                <ul>
                    <li class="menu-items menu-items-wh">
                        <a>Cine suntem</a>
                        <ul>
                            <li><a href="<?php print url('webhelp-roumanie-ro')?>">Webhelp România</a></li>
                            <li><a href="http://www.webhelp.com">Webhelp Grup</a></li>
                            <li><a href="<?php print url('pourquoi-webhelp')?>">De ce Webhelp</a></li>
                            <li><a href="<?php print url('actualites')?>">Articole </a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-avenir">
                        <a>Universul Webhelp</a>
                        <ul>
							<li><a href="<?php print url('sectoare-de-activitate')?>">Sectoare de activitate</a></li>
							<li><a href="<?php print url('profesii')?>">Profesii</a></li>
							<li><a href="<?php print url('universitatea-webhelp')?>">Formare</a></li>
							<li><a href="<?php print url('talent-management')?>">Evoluţie</a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-famille">
                        <a>Mediul de lucru Webhelp</a>
                        <ul>
                            <li><a href="<?php print url('well-being')?>">Well being</a></li>
                            <li><a href="<?php print url('avantaje-si-beneficii')?>">Avantaje si beneficii</a></li>
                            <li><a href="<?php print url('cultura-noastra')?>">Cultura noastră</a></li>
                            <li><a href="<?php print url('distractie')?>">Distracție</a></li>
                          <!--  <li><a href="<?php print url('well-being')?>">Solidaritate</a></li> -->
                        </ul>
                    </li>
                    <li class="menu-items menu-items-aventure">
                        <a>Alătură-ni-te</a>
                        <ul>
                            <li><a href="<?=url('<front>');?>#postuler-webhelp">Recrutare</a></li>
                            <li><a href="<?=url('<front>');?>#integration">Integrare</a></li>
                            <li><a href="<?php print url('offre-emploi')?>">Oportunități</a></li>
                            <li><a href="<?php print url('user/login')?>"> Alătură-ni-te</a></li>
                        </ul>
                    </li>
                </ul>
                  <?php }else{?>
							<ul>
                    <li class="menu-items menu-items-wh">
                        <a>Who We Are</a>
                        <ul>
                            <li><a href="<?php print url('webhelp-roumanie-en')?>">Webhelp Romania</a></li>
                            <li><a href="http://www.webhelp.com">Webhelp Intl</a></li>
                            <li><a href="<?php print url('pourquoi-webhelp')?>">Why Webhelp</a></li>
                            <li><a href="<?php print url('actualites')?>">Articles </a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-avenir">
                        <a>Working With Us</a>
                        <ul>
                            <li><a href="<?php print url('our-sectors')?>">Our Sectors</a></li>
                            <li><a href="<?php print url('our-people')?>">Our People</a></li>
                            <li><a href="<?php print url('webhelp-university')?>">Training</a></li>
                            <li><a href="<?php print url('talent-management')?>">Evolution</a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-famille">
                        <a>Webhelp Way of Working </a>
                        <ul>
                            <li><a href="<?php print url('work-environment')?>">Work Environment</a></li>
                            <li><a href="<?php print url('advantages-benefits')?>">Advantages & Benefits</a></li>
                            <li><a href="<?php print url('webhelp-culture')?>">Webhelp Culture</a></li>
                            <li><a href="<?php print url('well-being')?>">Well Being </a></li>
                          <!--   <li><a href="<?php print url('well-being')?>">Solidarity </a></li> -->
                        </ul>
                    </li>
                    <li class="menu-items menu-items-aventure">
                        <a>Join Us </a>
                        <ul>
                            <li><a href="<?=url('<front>');?>#postuler-webhelp">Recruitment</a></li>
                            <li><a href="<?=url('<front>');?>#integration">Integration</a></li>
                            <li><a href="<?php print url('offre-emploi')?>">Our opportunities</a></li>
                            <li><a href="<?php print url('user/login')?>">Join Us </a></li>
                        </ul>
                    </li>
                </ul>
					<?php }?>
                
            </div>
            <div class="suivez-nous">
                <div class="content-suivez-nous">
                    <span><?php print t('Follow us:'); ?></span>
                    <ul>
	                    <li><a target="_blank" href="https://www.facebook.com/webhelp.ro/"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://www.linkedin.com/in/webhelpromania"><i class="fa fa-linkedin"></i></a></li>
                        <li><a target="_blank" href="https://twitter.com/webhelpromania"><i class="fa fa-twitter"></i></a></li>
                        <li><a target="_blank" href="https://www.youtube.com/user/WebhelpTV"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom footer-bottom-pages">
        <div class="content-footer-bottom">
	        <?php if($lang_name=='fr'){ ?>
            <p class="text-center">Conformément aux principes décrits dans l’article 10 de la loi 67/98 du 26 octobre « loi sur la protection des données à caractère personnel », le candidat est informé de ce qui suit : les données personnelles recueillies dans les formulaires de demande seront traitées par Webhelp et sont destinées à être inclus dans : (i) les fichiers 'Webhelp Management, Administration, Communication et formation et (ii) 'Le personnel Webhelp reprend l'archivage et la sélection'. </p>
            <p class="copyright text-center">© 2017 Webhelp, All Rights Reserved. <a href="<?php print url('conditions-dutilisation'); ?>">Conditions d'utilisations </a></p>
            <?php }else{ ?>
            <p class="text-center">According to the terms and for the effects described on article 10th of Law 67/98, of October 26th (Law for Personal Data Protection), the candidate is informed of the following: the personal data gathered in the application forms will be treated by Webhelp and is destined to be included in: (i) ‘Webhelp’s Management, Administration, Communication and Personal Training’ files and in (ii) ‘Webhelp’s personnel resume archive and selection</p>
            <p class="copyright text-center">© 2017 Webhelp, All Rights Reserved. <a href="<?php print url('conditions-dutilisation'); ?>">Terms of use</a></p>
            <?php } ?>
        </div>
    </div>

</footer>
<div id="oldnavigator">
    <h2>Votre navigateur est obsolète.</h2>
    <p>Attention, sans la mise à jour de votre navigateur, <br>
        la visualisation des pages du site Webhelp Portugal ne sera pas <br>
        optimale ! Nous vous invitons à mettre à jour votre navigateur :</p>
    <div class="buttons">
        <a href="#"><img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-chrome.png"></a>
        <a href="#"><img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-ie.png"></a>
        <a href="#"><img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-safari.png"></a>
        <a href="#"><img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-firefox.png"></a>
        <a href="#"><img alt="" src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-opera.png"></a>
    </div>
</div>
<ul class="share-btn hidden" id="dvid">
    <li>
        <a href="<?php if ( $user->uid ) { print url("cv-webhelp"); }else{ print url("user/login"); } ?>">
            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mail.png" alt="">
        </a>
        <span><?php print t('Express Application'); ?></span>
    </li>
    <!-- <li>
            <a href="#chat">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/bubble.png" alt="">
            </a>
            <span>Pose tes questions, nous répondons</span>
        </li>
        <li>
            <a href="#call">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/call.png" alt="">
            </a>
            <span class="large">Tu veux passer ton entretien ?  <br>
                Nous te rappelons tout de suite,  <br>
                ou à <a href="#">l’heure qui t’arrange</a>
            </span>
        </li> -->
    <li class="list-share">
        <a href="#share">
            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/share.png" alt="">
        </a>
        <ul>
            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#" class="gplus"><i class="fa fa-google-plus"></i></a></li>
        </ul>
    </li>
    <li class="goto">
        <a class="scroll-top" href="#">
            <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/images/prev.png" alt="">
        </a>
    </li>
</ul>

<div class="popin" style="overflow-y: scroll">
    <a href="#" class="close-popin">×</a>
    <ul class="share-btn">
        <li>
            <a href="<?php print base_path();?>node/52863">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mail.png" alt="">
            </a>
        </li>
        <!--li>
                <a href="#chat">
                    <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/bubble.png" alt="">
                </a>
            </li-->
        <li>
            <a href="#call">
                <img src="<?php print base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/call.png" alt="">
            </a>
        </li>
    </ul>
    <div class="call">

        <div class="call-phone">
            <h2>Vous voulez passer votre <br> entretien ?</h2>
            <p style="margin-bottom: 50px;">Nous vous rappelons tout de suite : </p>
            <p class="cb-success" style="margin: -30px 0 20px 0; display: none;">Chargement...</p>
            <?php if ( $user->uid ) {
                $account = user_load($user->uid);
                ?>
                <form action="" method="post" name="callback-form" id="callback-form1">
                    <div class="formline first">
                        <input type="text" value="<?php print $account->field_nom["und"][0]["value"]?>" id="cnom" name="nom" disabled placeholder="Nom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="<?php print $account->field_prenom["und"][0]["value"]?>" id="cprenom" name="prenom" disabled placeholder="Prénom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="<?php print $user->mail?>" id="cemail" name="email" disabled placeholder="Email">
                    </div>
                    <div class="formline first">
                        <input type="text" value="<?php print $account->field_tel["und"][0]["value"]?>" id="cphone" name="cphone" placeholder="Téléphone">
                    </div>
                    <div class="formline first">
                        <select name="cregion" style="width: 100%;">
                            <option value="">Choisis une région</option>
                            <option value="Agadir" <?php if($account->field_region["und"][0]["value"]=="Agadir") { print 'selected'; } ?>>Agadir</option>
                            <option value="Fes" <?php if($account->field_region["und"][0]["value"]=="Fes") { print 'selected'; } ?>>Fès</option>
                            <option value="Kenitra" <?php if($account->field_region["und"][0]["value"]=="Kenitra") { print 'selected'; } ?>>Kénitra</option>
                            <option value="Meknes" <?php if($account->field_region["und"][0]["value"]=="Meknes") { print 'selected'; } ?>>Meknès</option>
                            <option value="RabatSale" <?php if($account->field_region["und"][0]["value"]=="RabatSale") { print 'selected'; } ?>>Rabat-Salé</option>
                        </select>
                    </div>

                    <input type="hidden" value="<?php print $account->uid?>" id="cuid" name="cuid" placeholder="">
                    <div class="formline">
                        <button type="submit" class="wb-btn">J’attends votre appel</button>
                    </div>
                </form>
            <?php }else{ ?>
                <form action="" method="post" name="callback-form" id="callback-form1">
                    <div class="formline first">
                        <input type="text" value="" id="cnom" name="cnom" placeholder="Nom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="" id="cprenom" name="cprenom" placeholder="Prénom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="" id="cemail" name="cemail" placeholder="Email">
                    </div>
                    <div class="formline first">
                        <input type="text" id="cphone" name="cphone" placeholder="Téléphone" value="">
                    </div>
                    <div class="formline first">
                        <input type="text" value="" id="cpassword" name="cpassword" placeholder="Mot de passe">
                    </div>
                    <div class="formline first">
                        <select name="cregion" style="width: 100%;">
                            <option value="">Choisis une région</option>
                            <option value="Agadir">Agadir</option>
                            <option value="Fes">Fès</option>
                            <option value="Kenitra">Kénitra</option>
                            <option value="Meknes">Meknès</option>
                            <option value="RabatSale">Rabat-Salé</option>
                        </select>
                    </div>
                    <div class="formline">
                        <button type="submit" class="wb-btn">J’attends votre appel</button>
                    </div>
                </form>
            <?php } ?>
            <!--a href="#" class="link">Choisir l’heure d’appel</a-->
            <p><br><br><strong>Horaires d’ouvertures hors jours fériés :</strong><br><br>

                Du Lundi au Vendredi<br>
                09h00 à 12h00<br>
                15h00 à 18h00<br></p>
        </div>

        <!--div class="call-hour">
            <h2>Choisis l’heure et la date qui te <br>conviennent :</h2>
            <form action="#" method="POST">
                <div class="formline time">
                    <select name="country" id="country" class="selectpicker">
                        <option value="23 sep">23 Sep</option>
                        <option value="24 sep">24 Sep</option>
                        <option value="25 sep">25 Sep</option>
                        <option value="26 sep">26 Sep</option>
                    </select>
                    <select name="country" id="country" class="selectpicker">
                        <option value="9">9:00</option>
                        <option value="10">10:00</option>
                        <option value="11">11:00</option>
                        <option value="12">12:00</option>
                    </select>
                </div>
                <div class="formline first">
                    <select name="country" id="country" class="popin-selectpicker">
                        <option value="+212">maroc</option>
                        <option value="+212">maroc</option>
                        <option value="+212">maroc</option>
                        <option value="+212">maroc</option>
                    </select>
                    <input type="text" value="+212" id="phone" name="phone">
                </div>
                <div class="formline">
                    <button type="submit" class="wb-btn">J’attends votre appel</button>
                </div>
            </form>
        </div-->

    </div>
    <div class="chat">

        <h2>Bienvenue</h2>

        <form action="#" method="POST">
            <div class="formline">
                <input type="text" id="name" name="name" placeholder="Nom">
            </div>
            <div class="formline">
                <input type="text" id="email" name="email" placeholder="Email">
            </div>
            <div class="formline submit">
                <button type="submit" class="wb-btn">Commencer le Tchat</button>
            </div>
        </form>
    </div>
    <div class="chatwindow">
        <h2>Pose tes questions, nous répondons</h2>
        <div class="box-chat">
            <div class="messages">
                <div class="push">
                    <span>Salam</span>
                </div>
                <div class="push left">
                    <span>Bonjour, Je suis ici, comment puis-je t'aider ?</span>
                </div>
            </div>

            <div class="zone">
                <textarea name="" id="" placeholder="Écris ton message ici"></textarea>
                <button><i class="fa fa-paper-plane"></i></button>
            </div>
        </div>
    </div>
</div> <!-- popin -->