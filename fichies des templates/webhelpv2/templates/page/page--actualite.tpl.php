<?php 
	global $language ;
	$lang_name = $language->language;
	$term = taxonomy_term_load($node->field_categorie_actualite[LANGUAGE_NONE][0][tid]);
	$name = $term->name;
?>
<header class="slide-pages">
    <div class="top-slide top-slide-actualites">
        <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        <div class="titre">
            <h1 class="text-center"><?php print t('News')?></h1>
            <!-- <h2 class="text-center" style="color: #fff; font-size: 45px"><?php print $name?></h3> -->
        </div>
    </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>

<div class="content-page"> 
	<div class=" top-page-content">
	     <?php 
	         print $messages; 
	         if($user->uid && in_array('administrator', $user->roles))
				 print l(t('Editer'), 'node/'.$node->nid.'/edit', array('attributes' => array('class' => 'editlink')));
	     ?>
		<div id="node-body">
			 <?php 
             print "<h1>".$node->title."</h1>"; 
			 print render($node->body[$lang_name][0][value]); ?>
            <div class="retour"><a href="<?php print base_path();?>actualites"><?php print t("Return to list"); ?></a></div>
		</div>
	</div>
</div>

<?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>


